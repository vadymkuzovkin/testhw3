class CustomError extends Error {
	constructor(message) {
		super(message);
		this.status = 500;
	}
}

class InvalidRequestError extends CustomError {
	constructor(message = 'Invalid request') {
		super(message);
		this.status = 400;
	}
}

module.exports = {
	CustomError,
	InvalidRequestError
};

exports.checkTruckDimensions = (type) => {
	if (type === 'SPRINTER') {
		return { length: 300, width: 250, height: 170, capacity: 1700 };
	}
	if (type === 'SMALL STRAIGHT') {
		return { length: 500, width: 250, height: 170, capacity: 2500 };
	}
	if (type === 'LARGE STRAIGHT') {
		return { length: 700, width: 350, height: 200, capacity: 4000 };
	}
};

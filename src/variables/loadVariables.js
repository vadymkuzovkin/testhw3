const loadState = ['En route to Pick Up', 'Arrived to Pick Up',
	'En route to delivery', 'Arrived to delivery'];
const loadStatus = ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'];

module.exports = {
	loadState,
	loadStatus
};

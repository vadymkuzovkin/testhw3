const mongoose = require('mongoose');
const { Schema } = mongoose;

const TruckSchema = new Schema({
	created_by: {
		type: Schema.Types.ObjectId,
		ref: 'User',
		required: true
	},
	assigned_to: {
		type: Schema.Types.ObjectId,
		ref: 'User',
		default: null
	},
	type: {
		type: String,
		enum: ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'],
		required: true
	},
	status: {
		type: String,
		enum: ['OL', 'IS'],
		default: 'IS'
	},
	dimensions: Object,

	created_date: {
		type: Date,
		default: Date.now()
	}
});

module.exports = mongoose.model('Truck', TruckSchema);

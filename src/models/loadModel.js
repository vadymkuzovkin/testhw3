const mongoose = require('mongoose');
const { loadState, loadStatus } = require('../variables/loadVariables');
const { Schema } = mongoose;

const LoadSchema = new Schema({
	created_by: {
		type: Schema.Types.ObjectId,
		ref: 'User',
		required: true
	},
	assigned_to: {
		type: Schema.Types.ObjectId,
		ref: 'User',
		default: null
	},
	status: {
		type: String,
		enum: [...loadStatus],
		default: 'NEW'
	},
	state: {
		type: String,
		enum: [...loadState, null],
		default: null
	},
	name: {
		type: String,
		required: true
	},
	payload: {
		type: Number,
		required: true
	},
	pickup_address: {
		type: String,
		required: true
	},
	delivery_address: {
		type: String,
		required: true
	},
	dimensions: {
		length: { type: Number },
		width: { type: Number },
		height: { type: Number }
	},
	logs: [{
		message: { type: String },
		time: { type: Date, default: Date.now() }
	}],

	created_date: {
		type: Date,
		default: Date.now()
	}
});

module.exports = mongoose.model('Load', LoadSchema);

const express = require('express');

const truckController = require('../controllers/truckController');

const router = express.Router();

router.get('/:id', truckController.getTruckByIdForUser);
router.put('/:id', truckController.updateTruckForUserById);
router.delete('/:id', truckController.deleteTruckForUserById);
router.post('/:id/assign', truckController.assignTruckToUserById);
router.get('/', truckController.getTrucksForUser);
router.post('/', truckController.postTruckForUser);

module.exports = router;

const express = require('express');

const userController = require('../controllers/userController');

const router = express.Router();

router.get('/', userController.getProfileForUser);
router.delete('/', userController.deleteProfileForUser);
router.patch('/password', userController.changePasswordForUser);

module.exports = router;

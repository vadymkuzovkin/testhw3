const express = require('express');

const authController = require('../controllers/authController');
const registrationValidation = require('../middlewares/registrationValidation');

const router = express.Router();

router.use(registrationValidation);

router.post('/register', authController.postRegister);
router.post('/login', authController.postLogin);

module.exports = router;

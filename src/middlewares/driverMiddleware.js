exports.driverMiddleware = (req, res, next) => {
	const { role } = req.user;
	if (role !== 'DRIVER') {
		return res.status(403).send(
			{ message: 'You are not allowed to interact with this page!' }
		);
	}
	next();
};

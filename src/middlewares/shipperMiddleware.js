exports.shipperMiddleware = (req, res, next) => {
	const { role } = req.user;
	if (role !== 'SHIPPER') {
		return res.status(403).send(
			{ message: 'You are not allowed to interact with this page!' }
		);
	}
	next();
};

const Joi = require('joi');

const registrationValidation = async (req, res, next) => {
	const schema = Joi.object({
		email: Joi.string()
			.email()
			.required(),
		password: Joi.string()
			.min(1)
			.max(20)
			.required(),
		role: Joi.string()
			.valid('DRIVER', 'SHIPPER')
			.optional()
	});

	try {
		await schema.validateAsync(req.body);
		next();
	} catch (error) {
		next(error);
	}
};

module.exports = registrationValidation;

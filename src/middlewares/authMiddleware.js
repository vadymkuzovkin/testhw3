const jwt = require('jsonwebtoken');
const { InvalidRequestError } = require('../utils/errorsUtil');

const authMiddleware = (req, res, next) => {
	const { authorization } = req.headers;

	if (!authorization) {
		throw new InvalidRequestError('Please provide \'authorization\' header!');
	}

	const token = authorization.split(' ')[1];

	if (!token) {
		throw new InvalidRequestError('Please provide correct token!');
	}

	try {
		const tokenPayload = jwt.verify(token, 'secret');
		req.user = {
			userId: tokenPayload._id,
			email: tokenPayload.email,
			role: tokenPayload.role
		};
		next();
	} catch (error) {
		res.status(401).send({ message: error.message });
	}
};

module.exports = authMiddleware;

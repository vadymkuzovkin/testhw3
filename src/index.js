const express = require('express');
const mongoose = require('mongoose');
const morgan = require('morgan');

const authMiddleware = require('./middlewares/authMiddleware');
const { driverMiddleware } = require('./middlewares/driverMiddleware');

const authRoutes = require('./routes/authRoutes');
const truckRoutes = require('./routes/truckRoutes');
const userRoutes = require('./routes/userRoutes');
const loadRoutes = require('./routes/loadRoutes');
const { CustomError } = require('./utils/errorsUtil');

require('dotenv').config();

const app = express();

app.use(morgan('tiny'));
app.use(express.urlencoded({ extended: true }));
app.use(express.json({ extended: true }));

app.use('/api/auth', authRoutes);
app.use(authMiddleware);
app.use('/api/users/me', userRoutes);
app.use('/api/trucks', driverMiddleware, truckRoutes);
app.use('/api/loads', loadRoutes);

app.use((req, res, next) => {
	res.status(404).send({ message: 'Page not found!' });
});

app.use((err, req, res, next) => {
	if (err instanceof CustomError) {
		return res.status(err.status).json({ message: err.message });
	}

	res.status(500).send({ message: err.message });
});

const start = async () => {
	try {
		await mongoose.connect('mongodb+srv://Vadym:5355493@firstcluster.risy7.mongodb.net/node_hw3?retryWrites=true&w=majority', {
			useNewUrlParser: true,
			useUnifiedTopology: true,
		});

		app.listen(process.env.PORT);
	} catch (error) {
		throw error.message;
	}
};

start();

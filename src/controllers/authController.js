const User = require('../models/userModel');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const { InvalidRequestError } = require('../utils/errorsUtil');
const asyncWrapper = require('../utils/asyncWrapper');

exports.postRegister = asyncWrapper(async (req, res, next) => {
	const { email, password, role } = req.body;
	const doesUserExists = await User.findOne({ email });

	if (doesUserExists) {
		throw new InvalidRequestError('User already exists');
	}

	const user = new User({
		email,
		password: await bcrypt.hash(password, 10),
		role,
	});

	await user.save();

	res.json({ message: 'Profile created successfully' });
});

exports.postLogin = asyncWrapper(async (req, res) => {
	const { email, password } = req.body;

	const user = await User.findOne({ email });

	if (!user) {
		throw new InvalidRequestError('User not found!');
	}

	if (!(await bcrypt.compare(password, user.password))) {
		throw new InvalidRequestError('Invalid login or password!');
	}

	const jwtToken = jwt.sign({
		_id: user._id,
		email: user.email,
		role: user.role,
	}, 'secret');

	return res.json({ jwt_token: jwtToken });
});

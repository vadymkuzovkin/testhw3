const Truck = require('../models/truckModel');
const { checkTruckDimensions } = require('../utils/checkTruckDimensions');
const { InvalidRequestError } = require('../utils/errorsUtil');
const asyncWrapper = require('../utils/asyncWrapper');

exports.postTruckForUser = asyncWrapper(async (req, res) => {
	const { userId } = req.user;

	const truckType = req.body.type;

	const truck = new Truck({
		created_by: userId,
		type: truckType,
		dimensions: checkTruckDimensions(truckType)
	});

	await truck.save();

	res.json({ message: 'Truck created successfully' });
});

exports.getTrucksForUser = asyncWrapper(async (req, res) => {
	const { userId } = req.user;

	const trucks = await Truck.find({ created_by: userId })
		.select('-__v -dimensions');

	res.json({ trucks });
});

exports.getTruckByIdForUser = asyncWrapper(async (req, res) => {
	const { userId } = req.user;
	const truckId = req.params.id;

	const truck = await Truck.findOne({ _id: truckId, created_by: userId })
		.select('-__v -dimensions');

	if (!truck) {
		throw new InvalidRequestError('Truck not found');
	}

	res.json({ truck });
});

exports.updateTruckForUserById = asyncWrapper(async (req, res) => {
	const { userId } = req.user;
	const truckId = req.params.id;
	const newType = req.body.type;

	const truck = await Truck.findOne({ _id: truckId, created_by: userId });

	if (truck.assigned_to) {
		throw new InvalidRequestError(
			'You can not update your truck while truck is assigned to you!'
		);
	}

	truck.type = newType;
	truck.dimensions = checkTruckDimensions(newType);

	await truck.save();

	res.json({ message: 'Truck details changed successfully' });
});

exports.deleteTruckForUserById = asyncWrapper(async (req, res) => {
	const { userId } = req.user;
	const truckId = req.params.id;

	const truck = await Truck.findOne({ _id: truckId, created_by: userId });

	if (truck.assigned_to) {
		throw new InvalidRequestError(
			'You can not delete your truck while truck is assigned to you!'
		);
	}

	await truck.remove();

	res.json({ message: 'Truck deleted successfully' });
});

exports.assignTruckToUserById = asyncWrapper(async (req, res) => {
	const { userId } = req.user;
	const truckId = req.params.id;

	await Truck.findOneAndUpdate(
		{ assigned_to: userId },
		{ $set: { assigned_to: null } }
	);

	await Truck.findOneAndUpdate(
		{ _id: truckId, created_by: userId },
		{ $set: { assigned_to: userId } }
	);

	res.json({ message: 'Truck assigned successfully' });
});

const Load = require('../models/loadModel');
const Truck = require('../models/truckModel');
const { loadState } = require('../variables/loadVariables');
const { InvalidRequestError } = require('../utils/errorsUtil');
const asyncWrapper = require('../utils/asyncWrapper');

exports.postLoadForUser = asyncWrapper(async (req, res) => {
	const loadInfo = { ...req.body };
	const { userId } = req.user;
	const load = new Load({
		created_by: userId,
		...loadInfo,
	});

	await load.save();

	res.json({ message: 'Load created successfully' });
});

// Check later
exports.getLoadsForUser = asyncWrapper(async (req, res) => {
	const { status } = req.query;
	let limit = req.query.limit || 10;
	if (limit > 50) {
		limit = 50;
	}
	const offset = req.query.offset || 0;

	const { userId, role } = req.user;
	if (role === 'DRIVER') {
		const loads = await Load.find({
			$or: [{ status: 'POSTED' }, { status: 'SHIPPED' }],
			assigned_to: userId,
		}).skip(offset).limit(limit);

		return res.json({ loads });
	}
	if (role === 'SHIPPER') {
		const loads = await Load.find({ created_by: userId })
			.skip(offset).limit(limit);
		return res.json({ loads });
	}
});

exports.getActiveLoadForUser = asyncWrapper(async (req, res) => {
	const { userId } = req.user;

	const load = await Load.findOne({ assigned_to: userId });

	if (!load) {
		throw new InvalidRequestError('You do not have active load yet');
	}

	res.json({ load });
});

exports.patchIterateToNextLoadState = asyncWrapper(async (req, res) => {
	const { userId } = req.user;

	const load = await Load.findOne({
		assigned_to: userId,
		status: { $ne: 'SHIPPED' }
	});

	if (!load) {
		throw new InvalidRequestError('You do not have active load yet');
	}

	if (load.status === 'SHIPPED') {
		throw new InvalidRequestError(
			'Load is already shipped, you can not change status'
		);
	}

	let newState;

	const loadStateMessage = `Load state changed to '${newState}'`;

	if (load.state === null) {
		newState = loadState[0];
	} else {
		const currStateIndex = loadState.findIndex((el) => el === load.state);
		newState = loadState[currStateIndex + 1];
		if (newState === loadState[3]) {
			load.status = 'SHIPPED';
			const truck = await Truck.findOne({ assigned_to: userId });
			truck.status = 'IS';
			await truck.save();
		}
	}
	load.logs.push({ message: loadStateMessage });
	load.state = newState;
	await load.save();

	res.json({ message: loadStateMessage });
});

exports.getLoadForUserById = asyncWrapper(async (req, res) => {
	const { userId, role } = req.user;
	const loadId = req.params.id;

	const load = await Load.findOne({ _id: loadId }).select('-__v');

	if (role === 'SHIPPER' && load.created_by != userId) {
		throw new InvalidRequestError('You do not have access to this load');
	}
	if (role === 'DRIVER' && load.assigned_to != userId) {
		throw new InvalidRequestError('You do not have access to this load');
	}

	res.json({ load });
});

exports.updateLoadForUser = asyncWrapper(async (req, res) => {
	const loadId = req.params.id;
	const { userId } = req.user;

	let load = await Load.findOne({ _id: loadId, created_by: userId });

	if (!load) {
		throw new InvalidRequestError('Load not found');
	}
	if (load.status !== 'NEW') {
		throw new InvalidRequestError(
			'You can not update load while it is in work'
		);
	}

	const updatedFields = { ...req.body };
	Object.assign(load, updatedFields)
	await load.save();

	res.json({ message: 'Load details changed successfully' });
});

exports.deleteLoadForUser = asyncWrapper(async (req, res) => {
	const loadId = req.params.id;
	const { userId } = req.user;

	const load = await Load.findOne({ _id: loadId, created_by: userId });

	if (!load) {
		throw new InvalidRequestError('Load not found');
	}

	if (load.assigned_to) {
		throw new InvalidRequestError(
			'You can not update load while it is in work'
		);
	}

	await load.remove();

	res.json({ message: 'Load deleted successfully' });
});

exports.postLoadForUserById = asyncWrapper(async (req, res) => {
	const loadId = req.params.id;
	const { userId } = req.user;

	const load = await Load.findOne({ _id: loadId, created_by: userId });

	if (!load) {
		throw new InvalidRequestError('Load not found');
	}
	load.status = 'POSTED';
	await load.save();

	const loadDimensions = load.dimensions;

	const truck = await Truck.findOne({
		'assigned_to': { $ne: null },
		'dimensions.width': { $gte: loadDimensions.width },
		'dimensions.length': { $gte: loadDimensions.length },
		'dimensions.length': { $gte: loadDimensions.length },
		'dimensions.capacity': { $gte: load.payload },
		'status': 'IS',
	});

	if (!truck) {
		load.status = 'NEW';
		await load.save();
		throw new InvalidRequestError('Driver not found');
	} else {
		load.status = 'ASSIGNED';
		load.state = loadState[0];
		load.logs.push({
			message: `Load assigned to driver with id ${truck.assigned_to}`
		});
		truck.status = 'OL';
		load.assigned_to = truck.assigned_to;

		await load.save();
		await truck.save();
		return res.json({
			message: 'Load posted successfully',
			driver_found: true,
		});
	}
});

exports.getLoadShippingInfoForUser = asyncWrapper(async (req, res) => {
	const { userId } = req.user;
	const loadId = req.params.id;

	const load = await Load.findOne({ _id: loadId, created_by: userId })
		.select('-__v');
	const truck = await Truck.findOne({ assigned_to: load.assigned_to })
		.select('-__v -dimensions');

	if (!load) {
		throw new InvalidRequestError('Load not found');
	}
	if (!truck) {
		throw new InvalidRequestError('Truck not found');
	}

	res.json({ load, truck });
});

module.exports = {
	"env": {
		"browser": true,
		"commonjs": true,
		"es2021": true
	},
	"extends": [
		"google"
	],
	"parserOptions": {
		"ecmaVersion": 12
	},
	"rules": {
		"indent": [2, "tab"],
		"no-tabs": 0,
		"object-curly-spacing": [2, "always"],
		"linebreak-style": ["error", "unix"],
		"comma-dangle": ["error", "only-multiline"],
		"new-cap": [2, { "capIsNewExceptions": ["Router"] }]
	}

};

import * as actionTypes from '../actions/actionTypes'

const initialState = {
	loads: [],
	loadFoundById: {},
	isDriverFound: null
}

const loadReducer = (state = initialState, action) => {
	switch (action.type) {
		case actionTypes.GET_LOADS:
			return {
				...state,
				loads: action.loads
			}
		case actionTypes.ADD_LOAD:
			return state
		case actionTypes.DELETE_LOAD:
			return state
		case actionTypes.POST_LOAD:
			return {
				...state,
				isDriverFound: true
			}
		case actionTypes.GET_LOAD_BY_ID:
			return {
				...state,
				loadFoundById: action.loadData
			}
		case actionTypes.UPDATE_LOAD:
			return state
		case actionTypes.POST_LOAD_FAIL:
			return {
				...state,
				isDriverFound: false
			}
		case actionTypes.SAVE_IS_DRIVER_FOUND:
			return {
				...state,
				isDriverFound: null
			}
		default: return state
	}
}

export default loadReducer
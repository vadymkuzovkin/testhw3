import * as actionTypes from '../actions/actionTypes'

const initialState = {
	token: null,
	loading: false,
	userData: {},
	error: ''
}

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case actionTypes.LOGIN:
			return {
				...state,
				token: action.token,
				userData: action.userData,
				error: ''
			};
		case actionTypes.LOGOUT:
			return {
				...state,
				token: null,
				userData: {},
				error: ''
			};
		case actionTypes.AUTH_FAIL:
			return {
				...state,
				error: action.err
			}
		case actionTypes.CHANGE_PASSWORD:
			return {
				...state,
				error: ''
			}
		case actionTypes.REGISTER:
			return {
				...state,
				error: ''
			}
		case actionTypes.DELETE_ACCOUNT:
			return {
				...state,
				error: '',
				token: null,
				userData: {}
			}

		default: return state;
	}
}

export default reducer;
import * as actionTypes from '../actions/actionTypes'

const initialState = {
	trucks: []
}

const truckReducer = (state = initialState, action) => {
	switch (action.type) {
		case actionTypes.GET_TRUCKS:
			return {
				...state,
				trucks: action.trucks
			}
		case actionTypes.ADD_TRUCK: return state

		default: return state
	}
}

export default truckReducer
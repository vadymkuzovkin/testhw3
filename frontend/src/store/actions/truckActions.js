import * as actionTypes from './actionTypes'
import $api from '../../http/http'

export const getTrucks = () => {
	return dispatch => {
		$api.get('/api/trucks')
			.then(res => dispatch(getTrucksSuccess(res.data.trucks)))
			.catch(err => console.log(err))
	}
}

export const getTrucksSuccess = (trucks) => {
	console.log(trucks)
	return {
		type: actionTypes.GET_TRUCKS,
		trucks
	}
}

export const addTruck = (truckType) => {
	return dispatch => {
		$api.post('/api/trucks', { type: truckType })
			.then(res => dispatch(addTruckSuccess()))
			.then(res => dispatch(getTrucks()))
			.catch(err => console.log(err))
	}
}

export const addTruckSuccess = () => {
	return {
		type: actionTypes.ADD_TRUCK
	}
}
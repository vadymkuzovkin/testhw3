// User actions
export const LOGIN = 'LOGIN'
export const REGISTER = 'REGISTER'
export const LOGOUT = 'LOGOUT'
export const AUTH_FAIL = 'AUTH_FAIL'
export const CHANGE_PASSWORD = 'CHANGE_PASSWORD'
export const DELETE_ACCOUNT = 'DELETE_ACCOUNT'

//Load actions
export const GET_LOADS = 'GET_LOADS'
export const ADD_LOAD = 'ADD_LOAD'
export const DELETE_LOAD = 'DELETE_LOAD'
export const CHANGE_LOAD = 'CHANGE_LOAD'
export const POST_LOAD = 'POST_LOAD'
export const GET_LOAD_BY_ID = 'GET_LOAD_BY_ID'
export const UPDATE_LOAD = 'UPDATE_LOAD'
export const POST_LOAD_FAIL = 'POST_LOAD_FAIL'
export const SAVE_IS_DRIVER_FOUND = 'SAVE_IS_DRIVER_FOUND'

//truck actions
export const GET_TRUCKS = 'GET_TRUCKS'
export const ADD_TRUCK = 'ADD_TRUCK'
import * as actionTypes from './actionTypes'
import jwtDecode from 'jwt-decode'
import axios from 'axios'
import $api from '../../http/http'

export const auth = (loc, regForm, logForm, history) => {
	return dispatch => {
		const ref = loc === '/register' ? '/api/auth/register' : '/api/auth/login'
		const reqBody = loc === '/register' ? regForm : logForm

		if (reqBody.email.trim() === '' || reqBody.password.trim() === '') {
			dispatch(authFail('Provide password and email!'))
		}

		axios.post(ref, {
			...reqBody
		}).then(res => {
			dispatch(loc === '/register' ? register() : login(res.data.jwt_token))
			if (loc === '/register') {
				history.push('/login')
			}
		})
			.catch(err => {
				console.log(err)
				dispatch(authFail(err))
			})
	}
}

export const login = (token) => {
	localStorage.setItem('token', token)
	const userData = jwtDecode(token)
	return {
		type: actionTypes.LOGIN,
		token,
		userData
	}
}

export const register = () => {
	return {
		type: actionTypes.REGISTER
	}
}

export const logout = () => {
	localStorage.removeItem('token')
	return {
		type: actionTypes.LOGOUT
	}
}

export const authFail = (err) => {
	return {
		type: actionTypes.AUTH_FAIL,
		err
	}
}

export const changePassword = (oldPassword, newPassword, history) => {
	return dispatch => {
		$api.patch('/api/users/me/password', {
			oldPassword,
			newPassword
		}).then(res => {
			dispatch(successPasswordChanged())
			history.push('/')
		})
			.catch(err => {
				if (Object.keys(err).length !== 0) {
					dispatch(authFail(err))
				}
			})
	}
}

export const successPasswordChanged = () => {
	return {
		type: actionTypes.CHANGE_PASSWORD
	}
}

export const deleteAccount = () => {
	return dispatch => {
		$api.delete('/api/users/me')
			.then(res => dispatch(successDeleteAccount()))
			.catch(err => console.log(err))
	}
}

export const successDeleteAccount = () => {
	localStorage.removeItem('token')
	return {
		type: actionTypes.DELETE_ACCOUNT
	}
}
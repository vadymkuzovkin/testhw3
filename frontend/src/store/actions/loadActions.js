import * as actionTypes from './actionTypes'
import axios from 'axios'
import $api from '../../http/http'

export const getLoads = () => {
	return dispatch => {
		$api.get('/api/loads')
			.then(res => dispatch(getLoadsSucccess(res.data.loads)))
			.catch(err => console.log(err))
	}
}

export const getLoadsSucccess = loads => {
	return {
		type: actionTypes.GET_LOADS,
		loads
	}
}

export const addLoad = (loadForm) => {
	return dispatch => {
		$api.post('/api/loads', { ...loadForm })
			.then(res => {
				dispatch(addLoadSuccess())
				dispatch(getLoads())
			})
			.catch(err => console.log(err))
	}
}

export const addLoadSuccess = () => {
	return {
		type: actionTypes.ADD_LOAD
	}
}

export const deleteLoad = (loadId) => {
	return dispatch => {
		$api.delete(`/api/loads/${loadId}`)
			.then(res => dispatch(deleteLoadSuccess()))
			.then(res => dispatch(getLoads()))
			.catch(err => console.log(err))
	}
}

export const deleteLoadSuccess = () => {
	return {
		type: actionTypes.DELETE_LOAD
	}
}

export const postLoad = (loadId) => {
	return dispatch => {
		$api.post(`/api/loads/${loadId}/post`)
			.then(res => dispatch(postLoadSuccess()))
			.then(res => dispatch(getLoads()))
			.catch(err => dispatch(postLoadFail()))
	}
}

export const postLoadSuccess = () => {
	return {
		type: actionTypes.POST_LOAD
	}
}

export const postLoadFail = () => {
	return {
		type: actionTypes.POST_LOAD_FAIL
	}
}

export const getLoadById = (loadId) => {
	return dispatch => {
		$api.get(`/api/loads/${loadId}`)
			.then(res => dispatch(getLoadByIdSuccess(res.data.load)))
			.catch(err => console.log(err))
	}
}

export const getLoadByIdSuccess = (loadData) => {
	return {
		type: actionTypes.GET_LOAD_BY_ID,
		loadData: loadData
	}
}

export const updateLoad = (load, loadId) => {
	return dispatch => {
		$api.put(`/api/loads/${loadId}`, load)
			.then(res => dispatch(updateLoadSuccess()))
			.then(res => dispatch(getLoads()))
			.catch(err => console.log(err))
	}
}

export const updateLoadSuccess = () => {
	return {
		type: actionTypes.UPDATE_LOAD
	}
}

export const saveIsDriverFound = () => {
	return {
		type: actionTypes.SAVE_IS_DRIVER_FOUND
	}
}
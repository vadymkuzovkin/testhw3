export const getTimeDifference = (time) => {
	const dateDiff = Math.round((Date.now() - new Date(time).getTime()) / 1000)
	const SECONDS_IN_HOUR = 3600
	const SECONDS_IN_DAY = 3600 * 24

	if (dateDiff < SECONDS_IN_HOUR) {
		return `${Math.round(dateDiff / 60)} ${Math.round() === 1 ? 'minute ago' : 'minutes ago'}`
	}
	if (dateDiff > SECONDS_IN_HOUR && dateDiff < SECONDS_IN_DAY) {
		return `${Math.round(dateDiff / 60 / 60)} ${Math.round(dateDiff / 60 / 60) === 1 ? 'hour ago' : 'hours ago'}`
	}
	if (dateDiff > SECONDS_IN_DAY) {
		return `${Math.round(dateDiff / 60 / 60 / 24)} ${Math.round(dateDiff / 60 / 60 / 24) === 1 ? 'day ago' : 'days ago'}`
	}
}
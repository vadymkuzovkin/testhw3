import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import LoadsHeader from '../../components/Loads/loadsHeader/loadsHeader'
import ErrorMessage from '../../components/errorMessage/errorMessage'
import * as actions from '../../store/actions'

const HistoryPage = (props) => {
	const [shippedLoads, setShippedLoads] = useState([])
	useEffect(() => {
		props.getLoads()
	}, [])


	return (
		<div className='page'>
			<h2 className='page_name'>History</h2>
			<LoadsHeader pageName='history' />
			<div className='loadsContent'>
				{shippedLoads.length === 0 ?
					<ErrorMessage>You do not have 'SHIPPED' loads yet.</ErrorMessage>
					: <div className='loadWrapper'>
						{shippedLoads.map(load =>
							<div key={load._id} className='load'>
								<p>{load.name}</p>
								<p>{load.status}</p>
								<p>{load.state}</p>
								<p>{load.pickup_address}</p>
								<p>{load.delivery_address}</p>
							</div>)}
					</div>}
			</div>
		</div>
	)
}

const mapStateToProps = state => {
	return {
		loads: state.load.loads
	}
}

const mapDisparchToProps = dispatch => {
	return {
		getLoads: () => dispatch(actions.getLoads())
	}
}

export default connect(mapStateToProps, mapDisparchToProps)(HistoryPage)

import React, { useState } from 'react'
import styles from './profile.module.css'
import { connect } from 'react-redux'
import Button from '../../components/UI/button/button'
import * as actions from '../../store/actions/userAction'
import Modal from '../../components/UI/modal/modal'

const Profile = (props) => {
	const [showPasswordModal, setShowPasswordModal] = useState(false)
	const [showDeleteAccModal, setShowDeleteAccModal] = useState(false)
	const [passwordForm, setPasswordForm] = useState({
		oldPassword: '',
		newPassword: ''
	})

	const onInputValueChange = (e) => {
		setPasswordForm({ ...passwordForm, [e.target.name]: e.target.value })
	}

	return (
		<div className='page'>
			<Modal clicked={() => setShowDeleteAccModal(false)} show={showDeleteAccModal} title='Do you want to delete your account?'>
				<div className={styles.deleteAccControls}>
					<Button btnType='btnDanger' clicked={() => props.deleteAccount()}>Delete</Button>
					<Button clicked={() => setShowDeleteAccModal(false)}>Cancel</Button>
				</div>
			</Modal>
			<Modal clicked={() => setShowPasswordModal(false)} show={showPasswordModal} title='Change password'>
				<label htmlFor='oldPassword'>Old Password</label>
				<input id='oldPassword'
					name='oldPassword'
					type='password'
					value={passwordForm.oldPassword}
					onChange={(e) => onInputValueChange(e)} />
				<label htmlFor='newPassword'>New Password</label>
				<input id='newPassword'
					name='newPassword'
					type='password'
					value={passwordForm.newPassword}
					onChange={(e) => onInputValueChange(e)} />
				{props.error ? <p>Old password is incorrect</p> : ''}
				<div className={styles.passModalControls}>
					<Button clicked={() => props.changePassword(passwordForm.oldPassword, passwordForm.newPassword, props.history)}>Change Password</Button>
					<Button btnType='btnDanger' clicked={() => setShowPasswordModal(false)}>Close</Button>
				</div>
			</Modal>
			<h2 className='page_name'>Profile</h2>
			<h4 className={styles.userRole}>{props.userData.role}</h4>
			<div className={styles.infoWrapper}>
				<div>
					<div>Email: <span className={styles.userData}>{props.userData.email}</span></div>
					{props.userData.userName ? <div>Name: <span className={styles.userData}>{props.userData.userName}</span></div> : ''}
				</div>

				<div className={styles.profileControls}>
					<Button clicked={() => setShowPasswordModal(true)}>Change Password</Button>
					<Button btnType='btnDanger' clicked={() => setShowDeleteAccModal(true)}>Delete Account</Button>
				</div>
			</div>
		</div >
	)
}

const mapStateToProps = state => {
	return {
		userData: state.user.userData,
		error: state.user.error
	}
}

const mapDispatchToProps = dispatch => {
	return {
		changePassword: (oldPassword, newPassword, history) => dispatch(actions.changePassword(oldPassword, newPassword, history)),
		deleteAccount: () => dispatch(actions.deleteAccount())
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Profile)


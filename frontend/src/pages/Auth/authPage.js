import React, { useState } from 'react'
import { connect } from 'react-redux'
import { useLocation } from 'react-router'
import styles from './authpage.module.css'
import * as actions from '../../store/actions/userAction'
import Button from '../../components/UI/button/button'
import { NavLink } from 'react-router-dom'

const AuthPage = (props) => {
	const [registerForm, setRegisterForm] = useState({
		email: '', password: '', role: 'DRIVER'
	})
	const [loginForm, setLoginForm] = useState({
		email: '', password: ''
	})
	const loc = useLocation().pathname

	const onAuth = (loc, registerForm, loginForm) => {
		props.auth(loc, registerForm, loginForm, props.history)
	}

	const inputChangedHandler = (e) => {
		if (loc === '/register') {
			setRegisterForm({ ...registerForm, [e.target.name]: e.target.value })
		} else {
			setLoginForm({ ...loginForm, [e.target.name]: e.target.value })
		}
	}

	return (
		<section className={styles.authForm_wrapper}>
			<div className={styles.authForm}>
				<h2 className={styles.authForm_title}>{loc === '/register' ? 'Register' : 'Login'}</h2>
				<div className={styles.form_control}>
					<label htmlFor='email'> Email</label>
					<input id='email' value={loc === '/register' ? registerForm.email : loginForm.email} name='email' type='email' onChange={e => inputChangedHandler(e)} />
				</div>
				<div className={styles.form_control}>
					<label htmlFor='password'>Password</label>
					<input id='password' value={loc === '/register' ? registerForm.password : loginForm.password} name='password' type='password' onChange={e => inputChangedHandler(e)} />
				</div>
				{loc === '/register' ? <div className={styles.form_control}>
					<label htmlFor='role'>Role</label>
					<select id='role' name='role' type='text' onChange={e => inputChangedHandler(e)} >
						<option value="DRIVER">DRIVER</option>
						<option value="SHIPPER">SHIPPER</option>
					</select>
				</div> : ''}
				{props.error ? <p>Invalid email or password.</p> : ''}
				<Button clicked={() => onAuth(loc, registerForm, loginForm)}>{loc === '/register' ? 'REGISTER' : 'LOGIN'}</Button>
				{loc === '/register' ? <p>Already have an account? <NavLink to='/login'>Login</NavLink></p>
					: <p>Do not have an account yet? <NavLink to='/register'>Register</NavLink></p>}
			</div >
		</section>
	)
}

const mapStateToProps = state => {
	return {
		redirect: state.user.redirect,
		error: state.user.error
	}
}

const mapDispatchToProps = dispatch => {
	return {
		auth: (loc, regForm, logForm, history) => dispatch(actions.auth(loc, regForm, logForm, history))
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(AuthPage)

import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import styles from './truckPage.module.css'
import * as actions from '../../../store/actions'
import ErrorMessage from '../../../components/errorMessage/errorMessage'
import { NavLink } from 'react-router-dom'
import Button from '../../../components/UI/button/button'
import { getTimeDifference } from '../../../utils/getTimeDiff'

const TruckPage = (props) => {
	const [chosenTruckType, setChosenTruckType] = useState('SPRINTER')

	useEffect(() => {
		props.getTrucks()
	}, [])

	return (
		<div className='page'>
			<h2 className='page_name'>Trucks</h2>
			<div className={styles.header}>
				<p>Type</p>
				<p>Status</p>
				<p>Created Date</p>
			</div>
			<div className={styles.trucksWrapper}>
				{props.trucks.length === 0 || !props.trucks ?
					<ErrorMessage>You do not have trucks yet.</ErrorMessage>
					: <div className='loadWrapper'>
						{props.trucks.map(truck =>
							<div key={truck._id} className='load'>
								<p>{truck.type}</p>
								<p>{truck.status}</p>
								<p>{getTimeDifference(truck.created_date)}</p>
								{truck.assigned_to && <p>{truck.assigned_to}</p>}
							</div>)}
					</div>}

			</div>
			<div className={styles.controlsWrapper}>
				<select className={styles.truckSelect} onChange={(e) => setChosenTruckType(e.target.value)}>
					<option value='SPRINTER' defaultValue>SPRINTER</option>
					<option value='SMALL STRAIGHT'>SMALL STRAIGHT</option>
					<option value='LARGE STRAIGHT'>LARGE STRAIGHT</option>
				</select>
				<Button clicked={() => props.addTruck(chosenTruckType)}>Add Truck</Button>
			</div>
		</div>
	)
}

const mapStateToProps = state => {
	return {
		trucks: state.truck.trucks
	}
}

const mapDispatchToProps = dispatch => {
	return {
		getTrucks: () => dispatch(actions.getTrucks()),
		addTruck: (truckType) => dispatch(actions.addTruck(truckType))
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(TruckPage)

import React, { useEffect, useState } from 'react'
import styles from './editLoad.module.css'
import Button from '../../../components/UI/button/button'
import { connect } from 'react-redux'
import * as actions from '../../../store/actions'

const SingleLoad = (props) => {
	const [loadForm, setLoadForm] = useState({
		name: '',
		payload: '',
		pickup_address: '',
		delivery_address: '',
		dimensions: {
			width: '',
			length: '',
			height: ''
		}
	})

	useEffect(() => {
		if (Object.keys(props.load).length !== 0) {
			const loadCopy = { ...props.load }
			const loadFormCopy = { ...loadForm }
			loadFormCopy.name = loadCopy.name
			loadFormCopy.payload = loadCopy.payload
			loadFormCopy.pickup_address = loadCopy.pickup_address
			loadFormCopy.delivery_address = loadCopy.delivery_address
			loadFormCopy.dimensions.width = loadCopy.dimensions.width
			loadFormCopy.dimensions.length = loadCopy.dimensions.length
			loadFormCopy.dimensions.height = loadCopy.dimensions.height
			setLoadForm(loadFormCopy)
		}

	}, [props.load])

	const onInputChange = (e) => {
		const name = e.target.name
		if (name === 'width' || name === 'height' || name === 'length') {
			const loadFormCopy = { ...loadForm }
			loadFormCopy.dimensions[e.target.name] = e.target.value
			setLoadForm(loadFormCopy)
		}
		setLoadForm({ ...loadForm, [e.target.name]: e.target.value })
	}

	return (
		<div className={[styles.loadWrapper, 'page'].join(' ')}>
			<h2 className='page_name'>Edit Load</h2>
			<div className={styles.inputsWrapper}>
				<div className={styles.modalControls}>
					<label htmlFor='name'>Name</label>
					<input id='name' name='name' value={loadForm?.name} type='text' onChange={(e) => onInputChange(e)} />
				</div>
				<div className={styles.modalControls}>
					<label htmlFor='name'>Payload</label>
					<input id='payload' name='payload' value={loadForm?.payload} type='number' onChange={(e) => onInputChange(e)} />
				</div>
				<div className={styles.modalControls}>
					<label htmlFor='pickup_address+'>Pickup Address</label>
					<input id='pickup_address' name='pickup_address' value={loadForm?.pickup_address} type='text' onChange={(e) => onInputChange(e)} />
				</div>
				<div className={styles.modalControls}>
					<label htmlFor='delivery_address'>Delivery Address</label>
					<input id='delivery_address' name='delivery_address' value={loadForm?.delivery_address} type='text' onChange={(e) => onInputChange(e)} />
				</div>
				<div className={styles.dimensionsWrapper}>
					<div className={styles.dimensionsTitle}>
						<p>Dimensions</p>
					</div>
					<div>
						<div className={styles.modalControls}>
							<label htmlFor='width'>Width</label>
							<input id='width' name='width' value={loadForm?.dimensions?.width} type='number' onChange={(e) => onInputChange(e)} />
						</div>
						<div className={styles.modalControls}>
							<label htmlFor='length'>Length</label>
							<input id='length' name='length' value={loadForm?.dimensions?.length} type='number' onChange={(e) => onInputChange(e)} />
						</div>
						<div className={styles.modalControls}>
							<label htmlFor='height'>Height</label>
							<input id='height' name='height' value={loadForm?.dimensions?.height} type='number' onChange={(e) => onInputChange(e)} />
						</div>
					</div>
				</div>
				<div className={styles.modalBtns}>
					<Button clicked={() => {
						props.updateLoad(loadForm, props.match.params.id)
						props.history.goBack()
					}}>Save load</Button>
					<Button clicked={() => props.history.goBack()} btnType='btnDanger'>Cancel</Button>
				</div>
			</div>
		</div>
	)
}

const mapStateToProps = state => {
	return {
		load: state.load.loadFoundById
	}
}

const mapDispatchToProps = dispatch => {
	return {
		updateLoad: (load, loadId) => dispatch(actions.updateLoad(load, loadId))
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(SingleLoad)

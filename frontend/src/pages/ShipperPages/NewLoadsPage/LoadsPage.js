import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import { NavLink } from 'react-router-dom'
import styles from './loadsPage.module.css'
import * as actions from '../../../store/actions/loadActions'
import Button from '../../../components/UI/button/button'
import Modal from '../../../components/UI/modal/modal'
import { getTimeDifference } from '../../../utils/getTimeDiff'
import ErrorMessage from '../../../components/errorMessage/errorMessage'
import LoadsHeader from '../../../components/Loads/loadsHeader/loadsHeader'
import Notification from '../../../components/Notification/notification'

const LoadsPage = (props) => {
	const [showAddLoadModal, setShowAddLoadModal] = useState(false)
	const [newLoads, setNewLoads] = useState([])
	const [showMessage, setShowMessage] = useState(false)
	const [loadForm, setLoadForm] = useState({
		name: '',
		payload: '',
		pickup_address: '',
		delivery_address: '',
		dimensions: {
			width: '',
			length: '',
			height: ''
		}
	})

	useEffect(() => {
		props.getLoads()
	}, [])

	useEffect(() => {
		if (props.isDriverFound !== null) {
			setShowMessage(true)
			setTimeout(() => {
				setShowMessage(false)
				props.saveIsDriverFound()
			}, 3000);
		}
	}, [props.isDriverFound])

	useEffect(() => {
		setNewLoads(props.loads.filter(load => load.status === 'NEW'))
	}, [props.loads])

	const onInputChange = (e) => {
		const name = e.target.name
		if (name === 'width' || name === 'height' || name === 'length') {
			const loadFormCopy = { ...loadForm }
			loadFormCopy.dimensions[e.target.name] = e.target.value
			setLoadForm(loadFormCopy)
		}
		setLoadForm({ ...loadForm, [e.target.name]: e.target.value })
	}

	let message;

	if (showMessage === true && props.isDriverFound === true) {
		message = 'Driver was found'
	}

	if (showMessage === true && props.isDriverFound === false) {
		message = 'Driver was not found'
	}

	return (
		<div className='page'>
			{showMessage ? <Notification>{message}</Notification> : ''}
			<Modal clicked={() => setShowAddLoadModal(false)} show={showAddLoadModal} title='Add Load'>
				<div className={styles.modalControls}>
					<label htmlFor='name'>Name</label>
					<input id='name' name='name' value={loadForm.name} type='text' onChange={(e) => onInputChange(e)} />
				</div>
				<div className={styles.modalControls}>
					<label htmlFor='name'>Payload</label>
					<input id='payload' name='payload' value={loadForm.payload} type='number' onChange={(e) => onInputChange(e)} />
				</div>
				<div className={styles.modalControls}>
					<label htmlFor='pickup_address+'>Pickup Address</label>
					<input id='pickup_address' name='pickup_address' value={loadForm.pickup_address} type='text' onChange={(e) => onInputChange(e)} />
				</div>
				<div className={styles.modalControls}>
					<label htmlFor='delivery_address'>Delivery Address</label>
					<input id='delivery_address' name='delivery_address' value={loadForm.delivery_address} type='text' onChange={(e) => onInputChange(e)} />
				</div>
				<div className={styles.dimensionsWrapper}>
					<div className={styles.dimensionsTitle}>
						<p>Dimensions</p>
					</div>
					<div>
						<div className={styles.modalControls}>
							<label htmlFor='width'>Width</label>
							<input id='width' name='width' value={loadForm.dimensions.width} type='number' onChange={(e) => onInputChange(e)} />
						</div>
						<div className={styles.modalControls}>
							<label htmlFor='length'>Length</label>
							<input id='length' name='length' value={loadForm.dimensions.length} type='number' onChange={(e) => onInputChange(e)} />
						</div>
						<div className={styles.modalControls}>
							<label htmlFor='height'>Height</label>
							<input id='height' name='height' value={loadForm.dimensions.height} type='number' onChange={(e) => onInputChange(e)} />
						</div>
					</div>
				</div>
				<div className={styles.modalBtns}>
					<Button clicked={() => {
						props.addLoad(loadForm, props.history)
						setShowAddLoadModal(false)
					}}>Add load</Button>
					<Button clicked={() => setShowAddLoadModal(false)} btnType='btnDanger'>Cancel</Button>
				</div>
			</Modal>
			<h2 className='page_name'>New Loads</h2>
			<LoadsHeader pageName='newLoads' />
			<div className='loadsContent'>
				{newLoads.length === 0 ?
					<ErrorMessage>You do not have 'NEW' loads yet.</ErrorMessage>
					: <div className='loadWrapper'>
						{newLoads.map(load =>
							<div key={load._id} className='load'>
								<p>{load.name}</p>
								<p>{getTimeDifference(load.created_date)}</p>
								<p>{load.pickup_address}</p>
								<p>{load.delivery_address}</p>
								<div className={styles.btnWrapper}>
									<Button title='Post load' clicked={() => props.postLoad(load._id)}>Post</Button>
									<NavLink onClick={() => props.getLoadById(load._id)}
										title='Change load'
										className={styles.changeLoad}
										to={`/new-loads/${load._id}`}>&#9998;</NavLink>
									<Button btnType='btnDanger' title='Delete load' clicked={() => props.deleteLoad(load._id)}>x</Button>
								</div>
							</div>)}
					</div>}
			</div>
			<div className={styles.loadsControls}>
				<Button clicked={() => setShowAddLoadModal(true)}>Add Load</Button>
			</div>
		</div >
	)
}

const mapStateToProps = state => {
	return {
		loads: state.load.loads,
		isDriverFound: state.load.isDriverFound
	}
}

const mapDispatchToProps = dispatch => {
	return {
		getLoadById: loadId => dispatch(actions.getLoadById(loadId)),
		postLoad: (loadId) => dispatch(actions.postLoad(loadId)),
		deleteLoad: (loadId) => dispatch(actions.deleteLoad(loadId)),
		getLoads: () => dispatch(actions.getLoads()),
		addLoad: (loadForm) => dispatch(actions.addLoad(loadForm)),
		saveIsDriverFound: () => dispatch(actions.saveIsDriverFound())
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(LoadsPage)

import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import * as actions from '../../../store/actions'
import LoadsHeader from '../../../components/Loads/loadsHeader/loadsHeader'
import ErrorMessage from '../../../components/errorMessage/errorMessage'

const AssignedLoadsPage = (props) => {
	const [assignedLoads, setAssignedLoads] = useState([])

	useEffect(() => {
		props.getLoads()
	}, [])

	useEffect(() => {
		setAssignedLoads(props.loads.filter(load => load.status === 'ASSIGNED'))
	}, [props.loads])

	return (
		<div class='page'>
			<h2 className='page_name'>Assigned Loads</h2>
			<LoadsHeader pageName='assignedLoadsPage' />
			<div className='loadsContent'>
				{assignedLoads.length === 0 ?
					<ErrorMessage>You do not have 'ASIGNED' loads yet.</ErrorMessage>
					: <div className='loadWrapper'>
						{assignedLoads.map(load =>
							<div key={load._id} className='load'>
								<p>{load.name}</p>
								<p>{load.status}</p>
								<p>{load.state}</p>
								<p>{load.pickup_address}</p>
								<p>{load.delivery_address}</p>
							</div>)}
					</div>}
			</div>
		</div>
	)
}

const mapStateToProps = (state) => {
	return {
		loads: state.load.loads
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		getLoads: () => dispatch(actions.getLoads())
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(AssignedLoadsPage)

import React from 'react'
import styles from './mainPage.module.css'
import Header from '../../components/UI/header/header'
import * as actions from '../../store/actions'
import { connect } from 'react-redux'
import SideDrawer from '../../components/UI/sideDrawer/sideDrawer'

const MainPage = (props) => {
	return (
		<div className={styles.wrapper}>
			<div className={styles.mainPage}>
				<Header clicked={() => props.onLogout()} email={props.userData.email} />
				<div className={styles.contentWrapper}>
					<SideDrawer role={props.userData.role} />
					<main className={styles.main}>
						{props.children}
					</main>
				</div>
			</div>
		</div>
	)
}

const mapStateToProps = state => {
	return {
		userData: state.user.userData
	}
}

const mapDispatchToProps = dispatch => {
	return {
		onLogout: (history) => dispatch(actions.logout())
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(MainPage)

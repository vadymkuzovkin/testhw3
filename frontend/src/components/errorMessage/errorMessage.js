import React from 'react'
import styles from './errorMessage.module.css'

const loadsMessage = (props) => {
	return (
		<p className={styles.message}><span>{props.children}</span></p>
	)
}

export default loadsMessage

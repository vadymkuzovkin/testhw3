import React from 'react'
import styles from './button.module.css'

const button = (props) => {
	return <button
		title={props.title}
		onClick={props.clicked}
		className={[styles.button, styles[props.btnType]].join(' ')}>{props.children}</button>
}

export default button

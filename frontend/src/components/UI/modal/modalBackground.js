import React from 'react'
import styles from './modal.module.css'

const modalBackground = (props) => {
	return (
		<div onClick={props.clicked} className={styles.modalBackground}>
			{props.children}
		</div>
	)
}

export default modalBackground

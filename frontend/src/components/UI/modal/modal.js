import React from 'react'
import Aux from '../../../hoc/Auxilliary'
import styles from './modal.module.css'
import ModalBg from './modalBackground'

const modal = (props) => {
	return (props.show ?
		< Aux >
			<ModalBg clicked={props.clicked} />
			<div className={styles.modal}>
				<p>{props.title}</p>
				<div className={styles.contentWrapper}>
					{props.children}
				</div>
			</div>
		</ Aux> : ''
	)
}

export default modal

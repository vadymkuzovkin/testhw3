import React from 'react'
import styles from './header.module.css'
import Button from '../button/button'

const header = (props) => {
	return (
		<header className={styles.header}>
			<p>Logo</p>
			<div className={styles.controls}>
				<p>Welcome {props.email}!</p>
				<Button clicked={props.clicked}>Logout</Button>
			</div>

		</header>
	)
}

export default header

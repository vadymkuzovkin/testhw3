import React from 'react'
import { NavLink } from 'react-router-dom'
import styles from './sideDrawer.module.css'

const sideDrawer = (props) => {
	return (
		<div className={styles.drawerWrapper}>
			<nav>

				{props.role === 'DRIVER' ? <NavLink to='/trucks'>TRUCKS</NavLink> : <NavLink to='/new-loads'>NEW LOADS</NavLink>}
				{props.role === 'DRIVER' ? <NavLink to='/active-load'>ACTIVE LOAD</NavLink> : <NavLink to='/assigned-loads'>ASSIGNED LOADS</NavLink>}
				<NavLink to='/history'>HISTORY</NavLink>
				<NavLink to='/profile'>PROFILE</NavLink>
			</nav>
		</div>
	)
}

export default sideDrawer

import React from 'react'
import Aux from '../../../hoc/Auxilliary';
import styles from './loadsHeader.module.css'

const loadsHeader = (props) => {
	let content;
	let style

	if (props.pageName === 'history' || props.pageName === 'assignedLoadsPage') {
		style = styles.loadsHeader
		content = <Aux>
			<p>Load Name</p>
			<p>Status</p>
			<p>State</p>
			<p>Pick-Up Address </p>
			<p>Delivery Address</p>
		</Aux>
	}
	if (props.pageName === 'newLoads') {
		style = styles.loadsHeaderNewLoads
		content = <Aux>
			<p>Load Name</p>
			<p>Created Date</p>
			<p>Pick-Up Address </p>
			<p>Delivery Address</p>
		</Aux>
	}

	return (
		<div className={style}>
			{content}
		</div>
	)
}

export default loadsHeader

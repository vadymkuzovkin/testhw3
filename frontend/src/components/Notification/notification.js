import React from 'react'
import styles from './notification.module.css'

const notification = (props) => {
	return (
		<div className={styles.notWrapper}>
			<p>{props.children}</p>
		</div>
	)
}

export default notification

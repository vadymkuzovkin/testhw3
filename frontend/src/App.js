import React from 'react'
import { useSelector } from 'react-redux'
import { Redirect, Route, Switch } from 'react-router'
import Aux from './hoc/Auxilliary'
import AuthPage from './pages/Auth/authPage'
import MainPage from './pages/Main/mainPage'
import Profile from './pages/Profile/Profile'
import LoadsPage from './pages/ShipperPages/NewLoadsPage/LoadsPage'
import EditLoadPage from './pages/ShipperPages/EditLoadPage/EditLoadPage'
import AssignedLoadsPage from './pages/ShipperPages/AssignedLoadsPage/AssignedLoadsPage'
import HistoryPage from './pages/History/HistoryPage'
import './pages/ShipperPages/sharedStyles.css'
import TruckPage from './pages/DriverPages/TruckPage/TruckPage'

const App = () => {
	const isAuth = useSelector(state => state.user.token) !== null
	const role = useSelector(state => state.user.userData).role

	let routes =
		<div>
			<Switch>
				<Route path={['/login', '/register']} component={AuthPage} />
				<Redirect to='/login' />
			</Switch>
		</div>

	if (isAuth && role === 'SHIPPER') {
		routes =
			<MainPage>
				<Switch>
					<Route exact path='/new-loads' component={LoadsPage} />
					<Route path='/new-loads/:id' component={EditLoadPage} />
					<Route path='/assigned-loads' component={AssignedLoadsPage} />
					<Route path='/history' component={HistoryPage} />
					<Route path='/profile' component={Profile} />
					<Redirect to='/new-loads' />
				</Switch>
			</MainPage>
	}

	if (isAuth && role === 'DRIVER') {
		routes =
			<MainPage>
				<Switch>
					<Route path='/trucks' component={TruckPage} />
					<Route path='/history' component={HistoryPage} />
					<Route path='/profile' component={Profile} />
					<Redirect to='/profile' />
				</Switch>
			</MainPage>
	}

	return (
		<Aux>
			{routes}
		</Aux>
	)
}

export default App
